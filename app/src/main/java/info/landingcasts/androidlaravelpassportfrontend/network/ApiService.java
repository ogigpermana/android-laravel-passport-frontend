package info.landingcasts.androidlaravelpassportfrontend.network;

import info.landingcasts.androidlaravelpassportfrontend.entities.AccessToken;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {
    @POST("register") // http://localhost/api/register
    @FormUrlEncoded
    Call<AccessToken>register(@Field("name") String name, @Field("email") String email, @Field("password") String password);
}
